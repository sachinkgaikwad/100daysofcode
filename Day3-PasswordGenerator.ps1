clear-host
Write-Host "`n`n"
$PassLength = "25"

$NumChars = (1..$PassLength) 

$Password = ForEach ($NumChar in $NumChars) {

-join (33, 42, (35..38) + (47..57) + (94..95) + (65..90) + (97..122) | get-random -count 1| % {[char]$_})

}

Write-Host "YOUR NEW PASSWORD:`n`n" -BackgroundColor Black -ForegroundColor Green

($Password | out-string).replace("`n","")

Write-Host "`n"