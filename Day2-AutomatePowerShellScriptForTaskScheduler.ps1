
#Variables
$iCUEProcess = get-process -name iCUE -ErrorAction SilentlyContinue | sort-object -Property ProcessName | Format-Table -Property ID, ProcessName, CPU 
$LightingService = Get-Service -DisplayName LightingService
$Empty = $null


Clear-Host
Write-Host "`n`niCue & Lighting Service Management`n`n" -ForegroundColor Yellow -BackgroundColor Black


function icue {


    If ($iCUEProcess -eq $Empty ) {

        Write-Host "`iCue is not currently running.`n" -BackgroundColor Black -ForegroundColor Red 

            $answer = Read-Host "Would you like to start iCue?(Y/N)"

                if ($answer -eq 'Y') {

                    Start-Process -FilePath "C:\Program Files (x86)\Corsair\CORSAIR iCUE Software\iCue.exe"

                    Write-Host "`niCue has been started." -BackgroundColor Black -ForegroundColor Green

                }

                

                Elseif ($answer -eq 'N') {

                    Write-Host "`niCue is still stopped." -BackgroundColor Black -ForegroundColor Red

                }

                

                Else {

                    Write-Host "`nDoes not compute, please try again.`n`n" -BackgroundColor Red -ForegroundColor White

                    icue

                }



    }


    Else {

                Write-Host "iCUE is currently running.`n" -BackgroundColor Black -ForegroundColor Green

                $answer = Read-Host "Would you like to stop iCue?(Y/N)"

                if ($answer -eq 'Y') {

                    Stop-Process -Name icue

                    Write-Host "`niCue has been stopped." -BackgroundColor Black -ForegroundColor Red

                }

                

                Elseif ($answer -eq 'N') {

                    Write-Host "`niCue is still running." -BackgroundColor Black -ForegroundColor Green

                }

                

                Else {

                    Write-Host "`nDoes not compute, please try again`n`n" -BackgroundColor Red -ForegroundColor White

                    icue

                }

        }




}



function lightingservice {


    If ($LightingService.status -eq "Running" ) {
        
        Write-Host "`LightingService is currently running.`n" -BackgroundColor Black -ForegroundColor Green

            $answer = Read-Host "Would you like to stop LightingService?(Y/N)"

                if ($answer -eq 'Y') {

                    Stop-Service -Name LightingService -Force

                    Write-Host "`nLightingService has been stopped." -BackgroundColor Black -ForegroundColor Red

                }

                

                Elseif ($answer -eq 'N') {

                    Write-Host "`nLightingService is still running." -BackgroundColor Black -ForegroundColor Green

                }

                

                Else {
                    
                    Write-Host "`nDoes not compute, please try again.`n`n" -BackgroundColor Red -ForegroundColor White

                    icue

                }



    }


    Else {

                Write-Host "LightingService is currently stopped.`n" -BackgroundColor Black -ForegroundColor Red

                $answer = Read-Host "Would you like to start LightingService?(Y/N)"

                if ($answer -eq 'Y') {

                    Start-Service -Name LightingService

                    Write-Host "`nLightingService has been started." -BackgroundColor Black -ForegroundColor Green

                }

                

                Elseif ($answer -eq 'N') {

                    Write-Host "`nLightingService is still stopped." -BackgroundColor Black -ForegroundColor Red

                }

                

                Else {

                    Write-Host "`nDoes not compute, please try again`n`n" -BackgroundColor Red -ForegroundColor White

                    icue

                }

        }




}

#Start icue function
icue

Write-Host "`n`n`n`n"

#Start lightingservice function
lightingservice


Write-Host "`n`n"
pause