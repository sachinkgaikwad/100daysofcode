##### https://ironscripter.us/building-a-powershell-command-inventory/

#####Intermediate
#####For this level, you are challenged to write something in PowerShell that will go through a directory of your scripts and count the number of lines of code you have written. This should include .ps1 and .psm1 files. Don’t worry about filtering out comments because those comments should be just as valuable as the code. But you should filter out blank and empty lines.

#####Your code should produce a result object that displays these property values:

    #####The parent path
    #####The total number of files
    #####The total lines of code and comments
    #####The report date

#####Naturally, you want to write the most efficient PowerShell code that you can to accomplish this goal.




$PowFiles = get-childitem -Path D:\OneDrive\Coding\100daysofcode\ -name *.ps1

ForEach ($PowerFile in $PowerFiles) {


$FileTable = get-content -Path D:\OneDrive\Coding\100daysofcode\$($PowFiles) | Measure-Object -line | Select-Object Lines, @{Name = "File Names"; Expression = {$PowerFile}}
$FileTable

}


