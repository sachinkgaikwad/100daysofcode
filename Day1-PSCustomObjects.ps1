$Computer = "Computer1","Computer2","Computer3"

Invoke-Command $Computer {


        #Top Mem Process & Usage
        $SingleMem = Get-WmiObject WIN32_PROCESS | Sort-Object -Property WorkingSetSize  -Descending | 
            Select-Object -First 1 PSComputerName,@{N="TopMemProcess";E={$_.ProcessName}}, @{Name="TopMemUsage";Expression={[math]::round($_.WorkingSetSize / 1mb)}} | 
            Sort-Object -Property @{Expression = "TopMemUsage";Descending = $True}
        
        
        #Total Mem Usage
        $os = Get-CimInstance Win32_OperatingSystem
        $MemTotal = ([math]::Round((($os.TotalVisibleMemorySize - $os.FreePhysicalMemory)/$os.TotalVisibleMemorySize)*100,2))
        
        
        #Top CPU Process & Usage
        $SingleCPU = Get-WmiObject WIN32_PerfFormattedData_PerfProc_Process | 
            Where-Object {$_.Name -ne "_Total" -and $_.Name -ne "Idle"} | 
            Sort-Object PercentProcessorTime -Descending | 
            Select-Object -First 1 @{Name="TopCPUprocess";Expression={$_.Name}},@{N="TopCPUusage";E={[Decimal]::Round(($_.PercentProcessorTime/$env:NUMBER_OF_PROCESSORS),2)}}
        
        
        #Total CPU Usage
        $cpuTotal = ([math]::Round((get-counter -Counter "\Processor(_Total)\% Processor Time" | 
            Select-Object -ExpandProperty countersamples | Select-Object -ExpandProperty cookedvalue),2))
        
        
        #Custom Object to output data
        $AllData = [PSCustomObject]@{
            SingleMem = $SingleMem;
            TopMemProcess = "TopMemProcess";
            TopMemUsage = "TopMemUsage";
            MemTotal = $MemTotal;
            SingleCPU = $SingleCPU;
            TopCPUprocess = "TopCPUprocess";
            TopCPUusage = "TopCPUusage";
            cpuTotal = $cpuTotal;
            
        }
        
        
        Return $AllData
        
} | 
Select-Object @{Name="Server Name";Expression={$_.SingleMem.PSComputerName}},
@{Name="Top Mem Process";Expression={$_.SingleMem.TopMemProcess}},
@{Name="Top Mem Usage (MB)";Expression={$_.SingleMem.TopMemUsage}},
@{Name="Total Memory Usage (%)";Expression={$_.MemTotal}},
@{Name="Top CPU Process";Expression={$_.SingleCPU.TopCPUprocess}},
@{Name="Top CPU Usage (MB)";Expression={$_.SingleCPU.TopCPUusage}},
@{Name="Total CPU Usage (%)";Expression={$_.cpuTotal}} |
Sort-Object -Property "Server Name" |
Out-GridView -Title "Mem & CPU Usage"