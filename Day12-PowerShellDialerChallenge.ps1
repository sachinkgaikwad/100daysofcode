
##### Final Script #####

Clear-Host

#assign letter groupings a number
$conversion = @{

    abc = 2;
    def = 3;
    ghi = 4;
    jkl = 5;
    mno = 6;
    pqrs = 7;
    tuv = 8;
    wxyz = 9
    
    }

#Letter groupings for matching
$groups = ("abc","def","ghi","jkl","mno","pqrs","tuv","wxyz")

#Telephone Keypad Reference Numbers/Letters
@'
  Text Dialer - Iron Scripter Challenge



             1     2    3
                  abc  def

             4     5    6
            ghi   jkl  mno

             7     8    9
            pqrs  tuv  wxyz


'@


do {

#Input word to convert to number here
$word = Read-Host "`n`nType Word"

#Splitting word into single letters for number conversion
$letters = $word.tochararray()

#The conversion from single letter to a number
$Number = foreach ($letter in $letters) {
$RegLetter = "[$letter]"
$match = $groups | where-object {$_ -match $RegLetter}
($conversion).$match

}

#All line breaks removed so numbers are on single line
$final = $($Number | out-string) -replace "`t|`n|`r",""

#Conversion output to Host
Write-Host "Word to Number Conversion: $final" -ForegroundColor Green

#'Enter' for continue or 'q' to quit
Write-Host "`nPress 'Enter' to Continue or Press 'q' to quit`n`n" -ForegroundColor Red -BackgroundColor Black
$readkey = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
$keypress = $readkey.character 

} until ($keypress -eq 'q')









##### Original Script #####


$conversion = @{

    abc = 2;
    def = 3;
    ghi = 4;
    jkl = 5;
    mno = 6;
    pqrs = 7;
    tuv = 8;
    wxyz = 9
}


$groupings = ("abc","def","ghi","jkl","mno","pqrs","tuv","wxyz")


$word = 'right'
$letters = $word.tochararray()

$AllLetters = foreach ($letter in $letters) {
"[$letter]"
}

$matchgroup = foreach ($Allletter in $AllLetters) {
$groupings | where-object {$_ -match $allletter}
}

$final = foreach ($match in $matchgroup){
($conversion).$match
}

($final | out-string) -replace "`t|`n|`r",""