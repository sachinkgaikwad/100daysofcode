Clear-Host
$PowerFiles = Get-ChildItem -Path D:\ -Recurse -Force -ErrorAction SilentlyContinue -name *.ps1

$Count = ($PowerFiles).count
$Date = Get-Date -format MM/dd/yyyy

Write-Output "Report Date: $Date"
Write-Output "Number of PowerShell Files: $Count"

ForEach ($PowerFile in $PowerFiles) {

get-item D:\$powerfile | select-object @{Name = 'Line Count';Expression = {(get-content -Path D:\$powerfile | Measure-Object -line).lines}},name,directory 


}
