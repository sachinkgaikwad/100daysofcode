
clear-host
Write-Host "`n`n++++++++++++++++++++++++++++++++++++++++++++++++++"
Write-Host "+                                                +"
Write-Host "+                                                +"
Write-Host "+              Password Generator                +"
Write-Host "+                                                +"
Write-Host "+                                                +"
Write-Host "++++++++++++++++++++++++++++++++++++++++++++++++++`n`n"

[int]$PassLength = Read-Host "Type password character length"


do {
clear-host
Write-Host "`n`n++++++++++++++++++++++++++++++++++++++++++++++++++"
Write-Host "+                                                +"
Write-Host "+                                                +"
Write-Host "+              Password Generator                +"
Write-Host "+                                                +"
Write-Host "+                                                +"
Write-Host "++++++++++++++++++++++++++++++++++++++++++++++++++`n`n"



$NumChars = (1..$PassLength) 

$Password = ForEach ($NumChar in $NumChars) {
    -join (33, 42, (35..38) + (47..57) + (94..95) + (65..90) + (97..122) | 
    get-random -count 1 | 
    ForEach-Object {[char]$_})
}

Write-Host "YOUR NEW PASSWORD:`n`n" -BackgroundColor Black -ForegroundColor Green

($password | out-string) -replace "`t|`n|`r",""
 
$readkey = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
$keypress = $readkey.character 


} until ($keypress -eq 'q')