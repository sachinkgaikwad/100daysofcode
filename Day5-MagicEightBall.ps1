Function EightBall {

Clear-Host
@'

        ____
    ,dP9CGG88@b,
  ,IP  _   Y888@@b,
 dIi  (_)   G8888@b
dCII  (_)   G8888@@b
GCCIi     ,GG8888@@@
GGCCCCCCCGGG88888@@@
GGGGCCCGGGG88888@@@@...
Y8GGGGGG8888888@@@@P.....
 Y88888888888@@@@@P......
 `Y8888888@@@@@@@P'......
    `@@@@@@@@@P'.......
       """""........

'@ 


$Rando = 
'As I see it, yes.',
'Ask again later.',
'Better not tell you now.',
'Cannot predict now.',
'Concentrate and ask again.',
'Do not count on it.',
'It is certain.',
'It is decidedly so.',
'Most likely.',
'My reply is no.',
'My sources say no.',
'Outlook not so good.',
'Outlook good.',
'Reply hazy, try again.',
'Signs point to yes.',
'Very doubtful.',
'Without a doubt.',
'Yes.',
'Yes, definitely.',
'You may rely on it.' 

$Random = Get-Random -InputObject $Rando
$Question = Read-Host "`n`nWhat would you like to know?"


Function Response {

Clear-Host

Write-Host "`n`n`n$Question" -ForegroundColor Magenta 
Start-Sleep -Milliseconds 2
Write-Host "$Random`n`n" -BackgroundColor Black 


Pause

}

Response

EightBall

}

EightBall
